# Scheduling Classes Using Genetic Algorithms

### Running the Application

* Clone the source code.
```
> git clone https://madawas@bitbucket.org/madawas/class-scheduling.git
```

*  Navigate to the project root and build the project.

```
> cd class-scheduling
> mvn clean install   
```

* Navigate to target and unzip `${project.name}-${project.version}.zip`

```
> unzip ${project.name}-${project.version}.zip  
```
* Run the application.
```
> java -jar ${project.name}-${project.version}.jar  
```


